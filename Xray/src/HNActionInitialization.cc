#include "HNActionInitialization.hh"
#include "HNPrimaryGeneratorAction.hh"
#include "HNRunAction.hh"
#include "HNEventAction.hh"
#include "HNSteppingAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNActionInitialization::HNActionInitialization()
 : G4VUserActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNActionInitialization::~HNActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HNActionInitialization::BuildForMaster() const
{
  HNRunAction* runAction = new HNRunAction;
  SetUserAction(runAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HNActionInitialization::Build() const
{
  SetUserAction(new HNPrimaryGeneratorAction);

  HNRunAction* runAction = new HNRunAction;
  SetUserAction(runAction);
  
  HNEventAction* eventAction = new HNEventAction(runAction);
  SetUserAction(eventAction);
  
  SetUserAction(new HNSteppingAction(eventAction));
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
