#include "HNDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

#include "Utility.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNDetectorConstruction::HNDetectorConstruction()
: G4VUserDetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNDetectorConstruction::~HNDetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* HNDetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
   
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 1.2*m;
  G4double world_sizeZ  = 1.2*m;
  // Could be changed to G4_Air in later simulation
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_Galactic");
  G4Box* solidWorld =    
    new G4Box("World",                       //its name
       0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size
      
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        world_mat,           //its material
                        "World");            //its name
                                   
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
  
  G4Element* elH = new G4Element("Hydrogen","H",1., 1.008*g/mole);
  G4Element* elC = new G4Element("Carbon","C", 6., 12.00*g/mole);
  G4Element* elN = new G4Element("Nitrogen","N", 7., 14.01*g/mole); 
  G4Element* elO = new G4Element("Oxygen","O",8.,16.00*g/mole);

  G4Material* Kapton = new G4Material( "Kapton",1.42*g/cm3 , 3);
  Kapton->AddElement(elC, 71.8916*perCent); 
  Kapton->AddElement(elN, 7.2902*perCent);
  Kapton->AddElement(elO, 20.8183*perCent);

  G4Material* Styrofoams = new G4Material("Styrofoams", 0.909*g/cm3, 2);
  Styrofoams->AddElement(elC, 92.2579*perCent);
  Styrofoams->AddElement(elH, 7.7421*perCent);
  
  G4Material* det_mat = Styrofoams;

  G4double det_sizeXY = 20*cm;
  G4double det_sizeZ  = 3*cm; 
  G4Box* solidDet =    
    new G4Box("Det",                    //its name
	      0.5*det_sizeXY, 0.5*det_sizeXY, 0.5*det_sizeZ); //its size

  G4LogicalVolume* logicDet =                         
    new G4LogicalVolume(solidDet,            //its solid
                        det_mat,             //its material
                        "Det");         //its name
               
  new G4PVPlacement(0,                       //no rotation
                    G4ThreeVector(0,0,0),    //at (0,0,0)
                    logicDet,                //its logical volume
                    "Det",                   //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking
  
  return physWorld;
}