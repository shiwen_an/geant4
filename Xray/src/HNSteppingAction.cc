#include "HNSteppingAction.hh"
#include "HNEventAction.hh"
#include "HNDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"


#include "SaveEnergyInfo.hh"
#include "Utility.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNSteppingAction::HNSteppingAction(HNEventAction* eventAction)
: G4UserSteppingAction(),
  fEventAction(eventAction)
{
  fb.open ("hit_result.txt",std::ios::out);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNSteppingAction::~HNSteppingAction()
{
  fb.close();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HNSteppingAction::UserSteppingAction(const G4Step* step)
{
  std::ostream os(&fb);
  
  // get volume of the current step
  G4LogicalVolume* volume 
    = step->GetPreStepPoint()->GetTouchableHandle()
    ->GetVolume()->GetLogicalVolume();
  G4int eventID = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

  if( volume->GetName() == "Det") {

    G4double edepStep = step->GetTotalEnergyDeposit();
    G4StepPoint* pre = step->GetPreStepPoint();
    G4StepPoint* post = step->GetPostStepPoint();
    G4Track* t = step->GetTrack();
    G4ThreeVector a = pre->GetPosition();
    G4ThreeVector b = post->GetPosition();
    G4int id = t->GetTrackID();

    fEventAction->addEdep(edepStep);  
    // if(debug_on) {
    //   G4cout<<"[DEBUG_ON] Event: "<<eventID<<" on Track: "<< id
    //    <<" E_Dep in Si: "<<edepStep<<"\n";
    //   G4cout<<"[DEBUG_ON] PreStep: "<< a.getX() <<" "<<a.getY()<<" "<<a.getZ()<<
    //   "\n[DEBUG_ON] PostStep: "<< b.getX() <<" "<< b.getY()<<" "<< b.getZ()<<"\n";
    // }
   
    os <<eventID<<","<<edepStep<<
      ","<<a.getX() <<","<<a.getY()<<","<<a.getZ()<<
      ","<<b.getX() <<","<<b.getY()<<","<<b.getZ()<<"\n";
    
  }


  /*
  if( volume->GetName() == "case_cover") {
    // collect energy deposited in this step
    G4double edepStep = step->GetTotalEnergyDeposit();
    fEventAction->addEdep(edepStep);  
    G4cout<<"[Stepping Action] Energy Deposition Value for case cover: "<<edepStep<<"\n";
  }
  */

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

