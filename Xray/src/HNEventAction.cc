#include "HNEventAction.hh"
#include "HNRunAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNEventAction::HNEventAction(HNRunAction* runAction)
: G4UserEventAction(),
  fRunAction(runAction)
{
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

HNEventAction::~HNEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HNEventAction::BeginOfEventAction(const G4Event*)
{    
  m_edep=0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void HNEventAction::EndOfEventAction(const G4Event*)
{   
  G4cout << m_edep << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
