// Author: Shiwen An
// Date: 2021/08/24
// Purpose: Run3 Makes Different Distribution 
//          For the different angular distribution 
//


#include "lib.hh"

using namespace std;

void run3(){
  vector<vector<double>> d1 = uniform_angle();
  display(d1);
  sequence_generation_angle(d1, "run3_E.mac");
  vector<vector<double>> d2 = parse2DCsvFile0("hit_result.txt");
  display(d2);
  vector<vector<double>> d3 = add_up_step(d2);
  vector<vector<double>> d4 = add_up_event_angle(d3, d1);
  TGraph * gr  = fill_graph0(d4, 0, 1);
  TGraph * gr1  = fill_graph1(d4, 0, 1);
  
  TCanvas* c1 = new TCanvas();
  gr->SetTitle(" G4 Incident Angle vs. Hit Prob. Silicon 20keV");
  gr->SetMarkerColor(1);
  gr->SetMarkerStyle(21); // With Square
  gr->Draw("ALP");
  gr->GetXaxis()->SetTitle(" Angle");
  gr->GetYaxis()->SetTitle("Probability");

  TCanvas* c2 = new TCanvas(); 
  c2->SetGridx();
  c2->SetGridy();
  gr1->SetTitle(" XCOM Angle vs. Hit Prob. Silicon 20keV");
  gr1->SetMarkerColor(2);
  gr1->SetMarkerStyle(kFullCircle); // With Square
  gr1->Draw("ALP");
  gr1->GetXaxis()->SetTitle(" Angle");
  gr1->GetYaxis()->SetTitle("Probability");

  TCanvas* c3 = new TCanvas(); 
  c3->SetGridx();
  c3->SetGridy();
  TMultiGraph  *mg  = new TMultiGraph();
  mg->Add(gr);
  mg->Add(gr1);
  mg->Draw("ALP");

}
