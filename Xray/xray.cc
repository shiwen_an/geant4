#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4RunManagerFactory.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh" 
#include "G4VisExtent.hh"

#include "HNPhysicsList.hh"
#include "HNDetectorConstruction.hh"
#include "HNActionInitialization.hh"
#include "G4RunManager.hh"
#include "Randomize.hh"
#include "Utility.hh"

bool gui_on = true;
bool geometry_on = true;
bool debug_on =false; 

int main(int argc, char* argv[])
{
  G4Random::setTheEngine(new CLHEP::MTwistEngine);
  G4UIExecutive* ui = 0;
  if ( argc == 1 ) {
    ui = new G4UIExecutive(argc, argv);
  }
  
  G4RunManager * runManager = new G4RunManager;
  runManager->SetUserInitialization(new HNDetectorConstruction());
  runManager->SetUserInitialization(new HNPhysicsList());
  runManager->SetUserInitialization(new HNActionInitialization());

  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
  
  G4UImanager* UImanager = G4UImanager::GetUIpointer();
  if ( ! ui ) { 
    // batch mode
    // Geometry off for detector testing
    geometry_on = false;
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  } else { 
    // interactive mode
    if(gui_on)UImanager->ApplyCommand("/control/execute init_vis.mac");
    if(debug_on) G4cout<<"[DEBUG_ON]UI Started \n";
    ui->SessionStart();
    delete ui;
  }
  
  delete visManager;
  delete runManager;

  return 0;
}



