// Author: Shiwen An
// Date: 2021/08/25
// Purpose: Run3 Makes Different Distribution 
//          For the different angular distribution 
//

#include "lib.hh"
using namespace std;

void run4(){
  double t;
  int p;
  vector<vector<double>> d1 = uniform_cone(49,90);
  sequence_generation_miniX(d1, "run4_dis.mac");
  vector<vector<double>> d2 = parse2DCsvFile0("hit_result.txt");
  vector<vector<double>> d3 = add_up_step_cone(d2);
  vector<vector<double>> d4 = add_up_event_cone(d3, d1);
  TCanvas* c0 = new TCanvas();
  TGraph2D * dt = new TGraph2D();
  for(int i=0; i< d1.size(); i++){
    dt->SetPoint(i, d1[i][0],d1[i][1],d1[i][2]);
    t = func(acos(d1[i][2])/PI*180);
    p = int(1000*t);
    std::cout<< d1[i][2]<<","<<t<<","<<p<<endl;
  }
  gStyle->SetPalette(55);
  dt->Draw("surf1");
  TCanvas* cc = new TCanvas();
  TGraph2D * dt1 = new TGraph2D();
  for(int i=0; i< d1.size(); i++){
    dt1->SetPoint(i, d1[i][0],d1[i][1], d4[i][2]);
  }
  gStyle->SetPalette(1);
  dt1->SetMarkerStyle(20);
  dt1->Draw("surf1");
}

