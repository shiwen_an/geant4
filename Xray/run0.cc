// Author: Shiwen An
// Date: July 22nd
// Purpose: Analyze the cross section 
//          and compare it with data in the silicon
//


using namespace std;

const int NOFEVENT = 1000;     // Number of beam at each energy level
const int ENERGY_MAX = 100; // loop from 0 to 1000 with interval of 10
const double ENERGY_STEP = 0.1;  // starts from
const bool DEBUG_ON = true;
const int col =2 ;  // column for the data structure


vector<vector<double>> parse2DCsvFile(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}

// Hard coding secton based on the energy level
// May be changed due to the energy ID could be direct output
vector<vector<double>> add_up_step (vector<vector<double>>& d) {

  int s = d.size();  // Size of the vector itself
  vector<vector<double >> pb; // push back vector 

  int a = 0 ; // set one index point
  double tmp = 0; // temporary value to store the energy deposition per event
  // loop through the raw data
  for (int i =0; i< s; i++){
      if(d[a][0] == d[i][0]){
        tmp += d[i][1];
      }
      else{
        pb.push_back({d[a][0], tmp});
        a=i;
        tmp = d[i][1];
      }

      //edge value
      if(i==(s-1)) pb.push_back({d[i][0],tmp});
  }

  int counter = 0 ;
  // Not all the event being recorded, same as energy level. 
  for(int i = 0; i< pb.size() ;i++){
    if(DEBUG_ON) {
        cout<<"Loop event"  <<pb[i][0]<< " and Value " << pb[i][1]<< " at index "
        << i <<"\n" ;
        if(pb[i][0 ] == 98 ) {
          counter++;
          cout<<"\n" <<"Energy Level at" <<counter<< "\n";
        }
    }
  }

  return pb;
}

// add up event function
// 2* 1000 vector, with 
// 1) Hit rates  Hits/Miss
// 2) Energy Deposition
//
vector<vector<double>> add_up_event(vector<vector<double>> & d){
  
  int s = d.size();
  int index_e = 1; // energy index 
  vector<vector<double>> d1;

  int h = 0;  // # of hit
  double tmp  = 0;

  // Loop with two edge cases
  for(int i = 0; i<s; i++){

    if(i == (s-1)){
      tmp += d[i][1];
      if(d[i][1] >0) h++; // hit then h++
      
      if(h<NOFEVENT) d1.push_back( {(double) h/(NOFEVENT-h), tmp, index_e* ENERGY_STEP} );
      else d1.push_back( {(double) NOFEVENT, tmp, index_e* ENERGY_STEP} );
      if(DEBUG_ON) std::cout<<i<<"and hit"<<h<< "Energy Level"<< index_e<<"\n";
      break;
    }

    if(i==0){
      tmp = d[i][1];
      if(tmp>0) h = 1;
      else h= 0;  // hit then h++
      if(DEBUG_ON) std::cout<<i<<" and hit is "<<h<<"\n";
    }else if(d[i][0]>d[i-1][0] ){
      if (d[i][1] > 0){tmp+=d[i][1];
      h++;
      if(DEBUG_ON) std::cout<<i<<" and hit is "<<h<<"\n";
      }
    }else if(d[i][0]<d[i-1][0]){
      if(h<NOFEVENT) d1.push_back( {(double) h/(NOFEVENT-h), tmp, index_e* ENERGY_STEP} );
      else d1.push_back( {(double) NOFEVENT, tmp, index_e* ENERGY_STEP} );
      if(DEBUG_ON) std::cout<<i<<"and hit is"<<h<< "Energy Level"<< index_e<<"\n";
      index_e++;
      tmp = d[i][1];
      if(tmp>0) h = 1; else h= 0;  // hit then h++
      if(DEBUG_ON) std::cout<<i<<" and hit is "<<h<<"\n";
    }
  }

  for(int j = 0; j<d1.size(); j++){
    if(DEBUG_ON) {
      std::cout<<"At Energy "<<d1[j][2]<<" Energy Deposit " << d1[j][1] 
               <<" and hit/miss " << d1[j][0] <<"\n"; 
    }
  }
  
  return d1;
}


// Cross Section * Probability for each energy level

// Energy Deposition vs. Energy Level Diagram

// Angular Distribution vs. Energy Level Diagram 

TH1D* fill_diagram(vector<vector<double>> &d, const char* name, const char* title,int bin_n, double xlow, double xup){
  TH1D*h = new TH1D(name,title,bin_n,xlow,xup);
  for(int i =0; i<d.size(); i++){
    if(DEBUG_ON) std::cout<<"At "<<i<<" Fill Value" << d[i][1] << "\n";
    h->Fill(d[i][1]); 
  }

  return h;
}

TGraph * fill_graph(vector<vector<double>> &d){
  int s = d.size();
  double x[s], y[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][2]; // Energy deposition
    y[i] = d[i][0]; // hit rate
    if(DEBUG_ON) cout<<x[i]<<y[i]<<"\n";
  }
  TGraph * g = new TGraph(s,x,y);

  return g;
}

void run0(){

  vector<vector<double>> d0 = parse2DCsvFile("hit_result.txt");
  vector<vector<double>> d1;
  for(int i = 0 ; i<d0.size(); i++) d1.push_back({d0[i][0], d0[i][1]});
  vector<vector<double>> d2 = add_up_step(d1);
  vector<vector<double>> d3 = add_up_event(d2);

  
  TH1D*h1 = fill_diagram(d1, "h1", "Step Energy Deposition Histogram", 10000, 0.0, 0.01);
  TH1D*h2 = fill_diagram(d2, "h2", "Event Energy Deposition Histogram", 10000, 0.00001, 0.2);
  
  TGraph* g1 = fill_graph(d3);
  // Not sure about what energy is ac
  //TH1D*h3 = diagram(d3, "h3", "Energy Level", ENERGY_LEVEL, 0.0, ENERGY_LEVEL*5);
  
  /*
  TCanvas* cc = new TCanvas();
  cc->Divide(2,2);
  cc->SetGridx();
  cc->SetGridy();
  cc->cd(1); 
  h1->Draw();
  cc->cd(2);
  h2->Draw();
  */

  TCanvas*c2 = new TCanvas();
  h2->Draw();

  TCanvas*c3 = new TCanvas();
  c3->cd();
  g1->SetTitle("Energy Level vs. Probability [Hit/Miss]");
  //g1->SetMarkerColor(4);
  //g1->SetMarkerStyle(8);
  g1->Draw("ALP");
  g1->GetXaxis()->SetTitle("keV");
  g1->Draw();
  

}
