//Author: Shiwen An
//Date: 2021/07/19
//Purpose: Read the data output from the GEANT4 and analyze 
//         1) Histogram for Energy Deposition / Event
//         2) Hit Efficiency # of events with hits/# of events w/o hits.
//Possible Online database for comparison
// Two Website LInk from NIST:
// https://physics.nist.gov/PhysRefData/Xcom/Text/chap2.html
// https://www.nist.gov/pml/xcom-photon-cross-sections-database
// Website link from Documents given as reference:
// https://www-nds.iaea.org/epdl97/ 
//

using namespace std;
const int NOFEVENT = 100;
const int ENERGY_LEVEL = 100;
const bool DEBUG_ON = true;

// read txt function
vector<vector<double>> parse2DCsvFile(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}

// load to histogram function
TH1D* load_data(vector<vector<double>>&d){
  int bin_n = 1000; //bin numbers 
  int s = d.size();
  double t1,t2, t3;
  double bin_s = 1e-05; //bin size 

  Double_t edges[bin_n];
  for(int i =0 ; i<bin_n;i++){
    edges[i]=i;
  }
  
  TH1D* h = new TH1D("h1","Energy Distribution Stepping Action",bin_n-1,edges);
  // Fill the step value w/o energy deposit
  for(int j = 0 ; j< s; j++) if( d.at(j).at(1)==0) h->Fill(0);
  
  // Fill the step value with energy deposit
  for(int i=0; i<bin_n;i++){
    for(int j = 0 ; j< s; j++){
      t1 =  (i-1);
      t2 =  i;
      t3 =  d.at(j).at(1)/bin_s;
      if( t1<t3 && t2>t3) {
        if(DEBUG_ON) std::cout<<"At "<<i<<" Fill Value" << t3 << "\n"; 
        h->Fill(edges[i]);
      }
    }
  }
  return h;
}

TH1D* load_event( vector<double>&d){
  int bin_n = 1000; //bin numbers 
  int miss = 0;
  int s = d.size();
  double t1,t2, t3;
  double bin_s = 1e-04; //bin size 

  Double_t edges[bin_n];
  for(int i =0 ; i<bin_n;i++){
    edges[i]=i;
  }
  
  TH1D* h = new TH1D("h2","Energy Distribution",bin_n-1,edges);
  // Fill the step value w/o energy deposit
  for(int j = 0 ; j< s; j++) if( d.at(j)==0) {
    h->Fill(0);
    miss++;
  }
  
  // Fill the step value with energy deposit
  for(int i=0; i<bin_n;i++){
    for(int j = 0 ; j< s; j++){
      t1 =  (i-1);
      t2 =  i;
      t3 =  d.at(j)/bin_s;
      if( t1<t3 && t2>t3) {
        if(DEBUG_ON) std::cout<<"At "<<i<<" Fill Value" << t3 << "\n"; 
        h->Fill(edges[i]);
      }
    }
  }
  std::cout<<"Events with hits: "<< (NOFEVENT-miss) << " Events Missed: " 
  <<miss<< "Hit Efficiency is "<<((NOFEVENT-miss) / (double)miss) << "\n" ;
  return h;
}

// add up event function
vector<double> add_up_event(vector<vector<double>> & d){
  // Hard Coding Section Included
  int n = NOFEVENT;
  int s = d.size();
  vector<double> d1 (n);
  for(int i = 0; i<s; i++){
    d1[d.at(i).at(0)] += d.at(i).at(1);
  }

  for(int j = 0; j<n; j++){
    if(DEBUG_ON) std::cout<<"At Event "<<j<<" Energy Deposit " << d1.at(j) << "\n"; 
  }
  
  return d1;
}


void analysis(){
  std::cout<<"GEANT4 Analysis"<<"\n";
  std::cout<<"Energy level"<<ENERGY_LEVEL<<"Number of Event is "<<NOFEVENT<<"\n";

  vector<vector<double>> d = parse2DCsvFile("hit_result.txt");
  int s = d.size();

  
  for(int i = 0; i<s; i++){
    for(int n = 0; n<NOFEVENT ; n++){
      if(DEBUG_ON && n== (int)d.at(i).at(0)) std::cout<<"[DEBUG_ON] Event "
          <<d.at(i).at(0)<<" Energy Deposition for Si Detector: "<<d.at(i).at(1)<<"\n";
    }
  }

  TCanvas* cc = new TCanvas();
  cc->Divide(2,1);
  cc->SetGridx();
  cc->SetGridy();
  cc->cd(1); 
  
  //This is the energy distribution for the 
  //Result from step action
  TH1D* h_energy = load_data(d);
  h_energy ->Draw();

  cc->cd(2);
  //Energy Distribution by Event
  vector<double> d1 = add_up_event(d);
  TH1D * h_energy_event = load_event(d1);
  h_energy_event->Draw();

  
}
