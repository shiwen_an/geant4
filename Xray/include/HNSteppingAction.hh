#ifndef HNSteppingAction_h
#define HNSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"

class HNEventAction;

class G4LogicalVolume;

/// Stepping action class
/// 

class HNSteppingAction : public G4UserSteppingAction
{
public:
  HNSteppingAction(HNEventAction* eventAction);
  virtual ~HNSteppingAction();
  
  // method from the base class
  virtual void UserSteppingAction(const G4Step*);
  
private:
  HNEventAction*  fEventAction;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif