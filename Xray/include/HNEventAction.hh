#ifndef HNEventAction_h
#define HNEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

class HNRunAction;

/// Event action class
///

class HNEventAction : public G4UserEventAction
{
public:
  HNEventAction(HNRunAction* runAction);
  virtual ~HNEventAction();
  
  virtual void BeginOfEventAction(const G4Event* event);
  virtual void EndOfEventAction(const G4Event* event);
  void addEdep(double ene) { m_edep+=ene; }
private:
  HNRunAction* fRunAction;
  double m_edep;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
