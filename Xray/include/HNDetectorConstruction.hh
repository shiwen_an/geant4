#ifndef HNDetectorConstruction_h
#define HNDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"


class G4VPhysicalVolume;
class G4LogicalVolume;


/// Detector construction class to define materials and geometry.

class HNDetectorConstruction : public G4VUserDetectorConstruction
{
public:
  HNDetectorConstruction();
  virtual ~HNDetectorConstruction();
  virtual G4VPhysicalVolume* Construct();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

