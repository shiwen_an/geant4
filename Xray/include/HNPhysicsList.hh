#ifndef HNFluoPhysicsList_h
#define HNPhysicsList_h 1

#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class G4VPhysicsConstructor;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class HNPhysicsList: public G4VModularPhysicsList
{
public:
  HNPhysicsList();
  virtual ~HNPhysicsList();

  void ConstructParticle();
  void ConstructProcess();    
  void AddDecay();
  void SetCuts();
    
private:
  G4String emName;
  G4VPhysicsConstructor*  emPhysicsList;
    
  G4double cutForGamma;
  G4double cutForElectron;
  G4double cutForPositron;    
  G4double cutForProton;    
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

