import sys, getopt
import pandas as pd
from pandas.core import indexing


s1 = "/gun/energy "
s2 = " MeV"
s3 = "/run/beamOn 1000"

# Integrate core part for the 


def main(argv):
   inputfile = ''
   outputfile = ''
   material = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:m:",["ifile=","ofile=","material="])
   except getopt.GetoptError:
      print('test.py -i <inputfile> -o <outputfile> -m <Al/Cu>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('test.py -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-m", "--material"):
         if arg == "Al" :
            material = arg
         elif arg == "Cu" :
            material = arg
         elif arg == "Kapton" :
            material = arg
         elif arg == "Styro" :
            material = arg
         elif arg == "Delrin" :
            material = arg
         else:
            print('No Available Material in the Lib for',arg)
            sys.exit(2)
   
   print('Input file is ', inputfile)
   print('Output file is ', outputfile)
   print('Material is ', material)

   fo = open(outputfile,"w")
   fi = open(inputfile)
   print(fi)

   cnt=0
   xv=[]
   yv=[]
   isValid=True
   Emin = 1e+3
   Emax = 1e+6
   nullw=' '*10
   targetMT = 501
   targetMF = 23
   if material == "Al" or material == "Cu" :      
      for line in fi:
         MAT=int(line[66:70])
         MF=int(line[70:72])
         MT=int(line[72:75])
         NS=int(line[75:80])
         if (MT==targetMT and MF == targetMF):
            cnt=cnt+1
            if(cnt<=3): 
               continue
            val=[]
            for i in range(6):
               sig=line[11*i+0:11*i+1]
               elem=line[11*i+1:11*(i+1)]
               if elem!=nullw:
                  elem=elem.replace('+','e+')
                  elem=elem.replace('-','e-')
                  word=sig+elem
                  val.append(float(word))
            for i,v in enumerate(val):
               if i%2==0:
                  if v>=Emin and v<=Emax:
                     print(v)
                     isValid=True
                  else:
                     isValid=False
                  if isValid:
                     xv.append(v*1e-6)
               else:
                 if isValid:
                     yv.append(v)
      for x,y in zip(xv,yv):
         print(s1+str(x)+s2+"\n"+s3, file=fo)
   if material == "Kapton" or material == "Styro" or material == "Delrin":
      for line in fi:
         print(s1+str(float(line[0:9]))+s2+"\n"+s3,file=fo)               
     
if __name__ == "__main__":
   main(sys.argv[1:])
