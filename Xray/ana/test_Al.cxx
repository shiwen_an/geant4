// Author: Shiwen An
// Date: 2021.10.25
// Purpose: Just testing and see what's inside the epdl database 
//          for real quick

// for the database
// the energy range 1 eV to 100 GeV. 
//

#include "lib.hh"

void test_Al(){

  // Total Cross Section With Coherent Scattering
  vector<vector<double>> d_ = parse2DCsvFile("xcom_Al.txt");
  vector<TGraph*> gr_list_ = root_plot(d_, THICKNESS_AL);
  TGraph*g1 = gr_list_[5];
  
  // Total Cross Section
  vector<vector<double>> d_0 = parse2DCsvFile_epdl("epdl_total.txt");
  vector<TGraph*> gr_list_0 = root_plot_epdl(d_0, THICKNESS_AL_EPDL);
  TGraph*g2 = gr_list_0[0];

  // TCanvas* cc = new TCanvas(); 
  // g1->SetTitle("XCOM Energy vs. Hit Prob. Al [200um/0.02cm] \n Total Cross Section w./ Coherent Scattering");
  // g1->SetMarkerColor(4);
  // g1->SetMarkerStyle(kFullCircle); // With Square
  // g1->GetXaxis()->SetTitle(" Energy [MeV]");
  // g1->GetYaxis()->SetTitle("Prob.");
  // g1->Draw("ALP");  

  TCanvas * c1 = new TCanvas();  
  c1->SetGridx();
  c1->SetLogx();
  c1->SetGridy();
  g2->SetTitle("EPDL Energy vs. Hit Prob. AL [200um] \n Total Cross Section");
  g2->SetMarkerColor(4);
  g2->SetMarkerStyle(kFullCircle); // With Square
  g2->GetXaxis()->SetTitle(" Energy [MeV]");
  g2->GetYaxis()->SetTitle("Prob.");
  g2->Draw("ALP");

  // Some codes for analyzing the hit result from G4
  // No need for sequence generation
  vector<vector<double>> d0 = parse2DCsvFile0("hit_result_Al.txt");
  
  vector<vector<double>> d1 = add_up_step(d0);
  display(d1);
  vector<vector<double>> d2 = add_up_event_epdl(d1, d_0);
  display(d2);
  TGraph * g3 = fill_graph(d2);
  TGraph * g4 = fill_graph_err(d2); // G4 Error
  TGraph * g5 = fill_difference_epdl(d_0, d2, THICKNESS_AL_EPDL);



  TCanvas * c3 = new TCanvas();
  c3->SetGridx();
  c3->SetGridy();
  c3->SetLogx();
  g3->SetTitle("Geant4 Energy vs. Hit Prob. Al");
  g3->SetMarkerColor(3);
  g3->SetMarkerStyle(kFullTriangleUp); // With Square
  g3->Draw("ALP");
  g3->GetXaxis()->SetTitle(" Energy [MeV]");
  // TGraph * g4 = fill_graph0(d,0,1);


  TCanvas * c2 = new TCanvas();
  c2->SetGridx();
  c2->SetGridy();
  c2->SetLogx();
  TMultiGraph  *mg  = new TMultiGraph();
  mg->Add(g2);
  mg->Add(g3); // Some hard coding
  mg->GetXaxis()->SetTitle(" Energy [MeV]");
  mg->GetYaxis()->SetTitle("Prob.");
  mg->GetHistogram()->SetTitle("EPDL vs. G4 ");
  mg->Draw("ALP");

  TCanvas* c4 = new TCanvas();
  c4->SetGridx();
  c4->SetGridy();
  c4->SetLogx();
  TMultiGraph  *mg_e  = new TMultiGraph();
  mg_e->Add(g5);
  mg_e->Add(g4);
  mg_e->GetXaxis()->SetTitle(" Energy [MeV]");
  mg_e->GetYaxis()->SetTitle("Prob. Error");
  mg_e->GetHistogram()->SetTitle("EPDL-G4[Red] vs. G4 Binomial [Black]");
  mg_e->Draw("ALP");
  

}
