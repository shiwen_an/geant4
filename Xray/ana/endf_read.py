#Author: Hajime Nanjo, Shiwen An 
#Date: 2021/10/29
#Purpose: read epdl database


# total 501
# with coherent scattering 502
# without coherent scattering 503

import sys, getopt
import pandas as pd
from pandas.core import indexing

def main(argv):
   ifn = ''
   ofn = "epdl_total.txt"
   targetMF = 23
   targetMT = 501
   cnt=0
   xv=[]
   yv=[]
   isValid=True
   Emin = 1e+3
   Emax = 1e+6
   nullw=' '*10
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('python3 endf_read.py -i <inputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('python3 endf_read.py -i <inputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         ifn = arg

   ofn = "epdl_total.txt"
   fi = open(ifn)
   fo = open(ofn,'w')
   for line in fi:
      MAT=int(line[66:70])
      MF=int(line[70:72])
      MT=int(line[72:75])
      NS=int(line[75:80])

      if(MF==targetMF and MT==targetMT):
         cnt=cnt+1
         if(cnt<=3):
             continue
         val=[]
         for i in range(6):
             sig=line[11*i+0:11*i+1]
             elem=line[11*i+1:11*(i+1)]
             if elem!=nullw:
                 elem=elem.replace('+','e+')
                 elem=elem.replace('-','e-')
                 word=sig+elem
                 val.append(float(word))
         for i,v in enumerate(val):
             if i%2==0:
                 if v>=Emin and v<=Emax:
                     print(v)
                     isValid=True
                 else:
                     isValid=False
                 if isValid:
                     xv.append(v)
             else:
                 #print(v)
                 if isValid:
                     yv.append(v)

   for x,y in zip(xv,yv):
      print(x,y,file=fo)
   fi.close()
   fo.close()

if __name__ == "__main__":
     main(sys.argv[1:])