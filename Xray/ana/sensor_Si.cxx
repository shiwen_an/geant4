// Author: Shiwen An
// Date: August 6th 2021
// Purpose: Read the XCOM data
// and compare XCOM data with the simulation from the GEANT4

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>


#define PI 3.1415926

const bool DEBUG_ON = false;
const int NOFEVENT = 1000;     // Number of beam at each energy level
const double ENERGY_STEP = 0.1;  // starts from atan (0.01/1) rad
const double THICKNESS = 0.03495; // The thickness of the silicon

using namespace std;
void display(vector<vector<double>> & d);
void sequence_generation(vector<vector<double>> & d);
vector<TGraph*> root_plot(vector<vector<double>> & d);
vector<vector<double>> parse2DCsvFile(string fileName);
vector<vector<double>> parse2DCsvFile0(string fileName);
vector<vector<double>> add_up_step (vector<vector<double>>& d);
vector<vector<double>> add_up_event(vector<vector<double>> & d,vector<vector<double>> & a);
TGraph * fill_graph(vector<vector<double>> &d);
TGraph * fill_graph0(vector<vector<double>> &d, int col1, int col2);
TGraph * fill_difference(vector<vector<double>> &d, vector<vector<double>> &d2);
TGraph * fill_graph_err(vector<vector<double>> &d);

void run2(){
  vector<vector<double>> d = parse2DCsvFile("xcom.txt");
  display(d);
  sequence_generation(d);
  vector<TGraph*> gr_list = root_plot(d);
  
  vector<vector<double>> d0 = parse2DCsvFile0("hit_result.txt");
  display(d0);
  // Add up energy deposition steps
  vector<vector<double>> d1 = add_up_step(d0);
  display(d1);
  vector<vector<double>> d2 = add_up_event(d1, d);
  TGraph * g1 = fill_graph(d2);
  TGraph * g2 = fill_graph0(d,0,1);
  TGraph * g3 = fill_difference(d,d2);
  TGraph * g4 = fill_graph_err(d2);

  TCanvas* cc = new TCanvas();
  cc->Divide(2,2);
  cc->cd(1);
  gr_list[6]->SetTitle("XCOM Energy vs. Hit Prob. Si");
  gr_list[6]->SetMarkerColor(4);
  gr_list[6]->SetMarkerStyle(kFullCircle); // With Square
  gr_list[6]->Draw("ALP");
  cc->cd(2);
  g1->SetTitle("Geant4 Energy vs. Hit Prob. Silicon");
  g1->SetMarkerColor(3);
  g1->SetMarkerStyle(kFullTriangleUp); // With Square
  g1->Draw("ALP");
  g1->GetXaxis()->SetTitle(" Energy [MeV]");
  cc->cd(3);
  TMultiGraph  *mg  = new TMultiGraph();
  mg->Add(g1);
  mg->Add(gr_list[6]); // Some hard coding
  mg->GetXaxis()->SetTitle(" Energy [MeV]");
  mg->SetTitle("Geant4 Silicon[Blue] vs. XCOM Database [Green]");
  mg->Draw("ALP");
  cc->cd(4);
  g3->SetTitle("Geant4 XCOM Prob. Difference");
  g3->SetMarkerColor(2);
  g3->SetMarkerStyle(21); // With Square
  g3->Draw("ALP");
  g3->GetXaxis()->SetTitle(" Energy [MeV]");

  TCanvas* c1 = new TCanvas();
  c1->SetGridx();
  c1->SetGridy();
  c1->SetLogx();
  gr_list[6]->Draw("ALP");

  TCanvas* c2 = new TCanvas();
  c2->SetGridx();
  c2->SetGridy();
  c2->SetLogx();
  g1->Draw("ALP");

  TCanvas* c3 = new TCanvas();
  c3->SetGridx();
  c3->SetGridy();
  c3->SetLogx();
  mg->Draw("ALP");

  TCanvas* c4 = new TCanvas();
  c4->SetGridx();
  c4->SetGridy();
  c4->SetLogx();
  g4->SetTitle("Geant4 Error Bar");
  g4->SetMarkerColor(1);
  g4->SetMarkerStyle(kFullCircle); // With circle
  TMultiGraph  *mg_e  = new TMultiGraph();
  mg_e->Add(g3);
  mg_e->Add(g4);
  mg_e->Draw("ALP");
}

TGraph * fill_difference(vector<vector<double>> &d, vector<vector<double>> &d2){
  Int_t n = d.size();
  double sum;
  Double_t x[n], y1[n], y2[n], y3[n], y4[n],
          y5[n], y6[n], y7[n], y_sum[n], y_diff[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0];
    y1[i] = 1-exp(-d[i][1]*THICKNESS) ;
    y2[i] = 1-exp(-d[i][2]*THICKNESS) ;
    y3[i] = 1-exp(-d[i][3]*THICKNESS) ;
    y4[i] = 1-exp(-d[i][4]*THICKNESS) ;
    y5[i] = 1-exp(-d[i][5]*THICKNESS) ;
    y6[i] = 1-exp(-d[i][6]*THICKNESS) ;
    y7[i] = 1-exp(-d[i][7]*THICKNESS) ;
    y_diff[i] = y7[i]-d2[i][1]; //XCOM - Simulation
  }
  TGraph *gr1 = new TGraph(n,x,y_diff);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(2);

  return gr1;
}



TGraph * fill_graph0(vector<vector<double>> &d, int col1, int col2){
  int s = d.size();
  double x[s], y[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][col1]; // Energy 
    y[i] = d[i][col2]; // Hit
    if(DEBUG_ON) cout<<x[i]<<y[i]<<"\n";
  }
  TGraph * g = new TGraph(s,x,y);

  return g;
}



TGraph * fill_graph(vector<vector<double>> &d){
  int s = d.size();
  double x[s], y[s], ex[s],ey[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][0]; // Energy 
    y[i] = d[i][1]; // Hit Probability
    ex[i] = 0 ;
    ey[i] = sqrt( double(y[i]*(1.0-y[i])/1000.0));
    cout<<ey[i]<<"\n";
  }
  TGraph * g = new TGraphErrors(s,x,y,ex,ey);
  return g;
}

TGraph * fill_graph_err(vector<vector<double>> &d){
  int s = d.size();
  double x[s], y[s], ex[s],ey[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][0]; // Energy 
    y[i] = d[i][1]; // Hit Probability
    ex[i] = 0 ;
    ey[i] = sqrt( double(y[i]*(1.0-y[i])/1000.0));
    cout<<ey[i]<<"\n";
  }
  TGraph * g = new TGraph(s,x, ey);
  return g;
}

vector<vector<double>> add_up_event(vector<vector<double>> & d, vector<vector<double>> & a){
  
  int s = d.size();
  int index_e = 1; // energy index 
  int h = 0;       // # of hit
  double tmp  = 0;
  vector<vector<double>> d1;
  for(int i = 0; i<s; i++){
    if(i == (s-1)){
      tmp += d[i][1];
      if(d[i][1] >0) h++; // hit then h++
      
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp} );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      break;
    }

    if(i==0){
      tmp = d[i][1];
      if(tmp>0) h = 1;
      else h= 0;  // hit then h++
    }else if(d[i][0]>d[i-1][0] ){
      if (d[i][1] > 0){tmp+=d[i][1];
        h++;
      }
    }else if(d[i][0]<d[i-1][0]){
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp } );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      index_e++;
      tmp = d[i][1];
      if(tmp>0) h = 1; else h= 0;  // hit then h++
    }
  }

  for(int j = 0; j<d1.size(); j++){
    if(DEBUG_ON) {
      std::cout<<"At Energy "<<d1[j][2]<<" Energy Deposit " << d1[j][1] 
               <<" and hit/miss " << d1[j][0] <<"\n"; 
    }
  }
  int x1 = a.size();
  int x2 = d1.size();

  if( x1 == x2 ) std::cout<<"Great!"<<endl;
  
  for(int i =0; i<x1; i++){
    d1[i][0] = a[i][0];
  }

  return d1;
}

vector<vector<double>> add_up_step (vector<vector<double>>& d) {

  int s = d.size();  
  vector<vector<double >> pb; 
  int a = 0 ; 
  double tmp = 0; 
  for (int i =0; i< s; i++){
      if(d[a][0] == d[i][0]){
        tmp += d[i][1];
      }
      else{
        pb.push_back({d[a][0], tmp});
        a=i;
        tmp = d[i][1];
      }
      if(i==(s-1)) pb.push_back({d[i][0],tmp});
  }

  int counter = 0 ;
  for(int i = 0; i< pb.size() ;i++){
    if(DEBUG_ON) {
        cout<<"Loop event"  <<pb[i][0]<< " and Value " << pb[i][1]<< " at index "
        << i <<"\n" ;
        if(pb[i][0 ] == 98 ) {
          counter++;
          cout<<"\n" <<"Energy Level at" <<counter<< "\n";
        }
    }
  }

  return pb;
}

//
// Input XCOM data and 
// Gives back the attenuation value
// Density of Silicon gives 2.33 g/cm^3
// the thickness for silicon plate is 0.150 mm
// which is 0.0150 cm 
// then the X value is roughly 0.03495
// 
vector<TGraph*> root_plot(vector<vector<double>> & d){
  Int_t n = d.size();
  double sum;
  Double_t x[n], y1[n], y2[n], y3[n], y4[n],
          y5[n], y6[n], y7[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0];
    y1[i] = 1-exp(-d[i][1]*THICKNESS) ;
    y2[i] = 1-exp(-d[i][2]*THICKNESS) ;
    y3[i] = 1-exp(-d[i][3]*THICKNESS) ;
    y4[i] = 1-exp(-d[i][4]*THICKNESS) ;
    y5[i] = 1-exp(-d[i][5]*THICKNESS) ;
    y6[i] = 1-exp(-d[i][6]*THICKNESS) ;
    y7[i] = 1-exp(-d[i][7]*THICKNESS) ;
  }
  
  TGraph *gr1 = new TGraph(n,x,y1);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(1);

  TGraph *gr2 = new TGraph(n,x,y2);
  gr2->SetMarkerStyle(21); // With Square
  gr2->SetMarkerColor(2);
  TGraph *gr3 = new TGraph(n,x,y3);
  gr3->SetMarkerStyle(21); // With Square
  gr3->SetMarkerColor(3);
  
  TGraph *gr4 = new TGraph(n,x,y4);
  gr4->SetMarkerStyle(21); // With Square
  gr4->SetMarkerColor(4);

  TGraph *gr5 = new TGraph(n,x,y5);
  gr5->SetMarkerStyle(21); // With Square
  gr5->SetMarkerColor(5);

  TGraph *gr6 = new TGraph(n,x,y6);
  gr6->SetMarkerStyle(21); // With Square
  gr6->SetMarkerColor(6);

  TGraph *gr7 = new TGraph(n,x,y7);
  gr7->SetMarkerStyle(21); // With Square
  gr7->SetMarkerColor(7);

  vector<TGraph*> gr_list;
  gr_list.push_back(gr1);
  gr_list.push_back(gr2);
  gr_list.push_back(gr3);
  gr_list.push_back(gr4);
  gr_list.push_back(gr5);
  gr_list.push_back(gr6);
  gr_list.push_back(gr7);
  //TCanvas *c1 = new TCanvas ("c1","",
                               //s200,10,600,400);

  // draw the graph with the axis,contineous line, and put
  // a marker using the graph's marker style at each point
  //gr1->SetMarkerStyle(21);
  //c1->cd();
  //gr1->Draw("APL");
  std::cout<<endl;

  if(DEBUG_ON){
  TMultiGraph  *mg  = new TMultiGraph();
    mg->Add(gr1);
    mg->Add(gr2);
    mg->Add(gr3);
    mg->Add(gr4);
    mg->Add(gr5);
    mg->Add(gr6);
    mg->Add(gr7);
    mg->Draw("ALP");
  }
  return gr_list;

}

//
// Generate Energy For the Plot
//
void sequence_generation(vector<vector<double>> & d){
  std::filebuf fb;
  fb.open ("run2_E.mac",std::ios::out);

  std::ostream os(&fb);
  double a = 0 ;
  double r = 0.001;
  double t = 0;
  /*
  while ( a<100)
  {
    a = r*exp(t); 
    std::cout<<a<<endl;
    os<<"/gun/energy "<<a<<" MeV"<<endl;
    t+= r;
  }
  */

  // Energy following the data
  for(int i= 0; i< d.size(); i++){
    os<<"/gun/energy "<<d[i][0]<<" MeV"<<endl;
    os<<"/run/beamOn 1000"<<endl;
  }
  //
  // two command with generation of 
  // different energy with 1000 colliding particle
  // /gun/energy {gun_energy} keV 
  // /run/beamOn 1000
  // 
  fb.close();
}

//
// Get Function
//
void display(vector<vector<double>> & d){
  int s = d.size();
  int s_r = d[0].size();
  for(int n= 0 ; n<s; n++){
    for(int m = 0 ; m<s_r; m++){
      std::cout<<d[n][m]<<" ";
    }
    std::cout<<std::endl;
  }
}

//
// Common Parse Function for comma txt file
//
vector<vector<double>> parse2DCsvFile0(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}

//
// Common Parse Function for space csv file
//
vector<vector<double>> parse2DCsvFile(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ' '))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}
