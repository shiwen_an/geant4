// Author: Shiwen An
// Date: 2021.11.04
// Purpose: Just testing and see what's inside the epdl database 
//          for real quick


#include "lib.hh"

void test_Delrin(){

  // Total Cross Section With Coherent Scattering
  vector<vector<double>> d_ = parse2DCsvFile("xcom_CH2O.txt");
  vector<TGraph*> gr_list_ = root_plot(d_, THICKNESS_DELRIN);
  TGraph*g1 = gr_list_[6];
  
  // Total Cross Section
  // vector<vector<double>> d_0 = parse2DCsvFile_epdl("epdl_total.txt");
  // vector<TGraph*> gr_list_0 = root_plot_epdl(d_0, THICKNESS_CU_EPDL);
  // TGraph*g2 = gr_list_0[0];

  TCanvas* cc = new TCanvas(); 
  cc->SetGridx();
  cc->SetGridy();
  cc->SetLogx();
  g1->SetTitle("XCOM Energy vs. Hit Prob. Delrin [200um/0.02cm] \n Total Cross Section w./ Coherent Scattering");
  g1->SetMarkerColor(4);
  g1->SetMarkerStyle(kFullCircle); // With Square
  g1->GetXaxis()->SetTitle(" Energy [MeV]");
  g1->GetYaxis()->SetTitle("Prob.");
  g1->Draw("ALP");  

  // TCanvas * c1 = new TCanvas();  
  // c1->SetGridx();
  // c1->SetLogx();
  // c1->SetGridy();
  // g2->SetTitle("EPDL Energy vs. Hit Prob. Cu [20um] \n Total Cross Section");
  // g2->SetMarkerColor(4);
  // g2->SetMarkerStyle(kFullCircle); // With Square
  // g2->GetXaxis()->SetTitle(" Energy [MeV]");
  // g2->GetYaxis()->SetTitle("Prob.");
  // g2->Draw("ALP");

  // Some codes for analyzing the hit result from G4
  // No need for sequence generation
  vector<vector<double>> d0 = parse2DCsvFile0("hit_result_Delrin.txt");  
  vector<vector<double>> d1 = add_up_step(d0);

  // Based on Database change these two
  // vector<vector<double>> d2 = add_up_event_epdl(d1, d_);
  vector<vector<double>> d2 = add_up_event(d1, d_);
  TGraph * g3 = fill_graph(d2);
  TGraph * g4 = fill_graph_err(d2); // G4 Error

  //TGraph * g5 = fill_difference_epdl(d_, d2, THICKNESS_Styro);
  TGraph * g5 = fill_difference(d_, d2, THICKNESS_DELRIN);

  TCanvas * c3 = new TCanvas();
  c3->SetGridx();
  c3->SetGridy();
  c3->SetLogx();
  g3->SetTitle("Geant4 Energy vs. Hit Prob. Delrin");
  g3->SetMarkerColor(3);
  g3->SetMarkerStyle(kFullTriangleUp); // With Square
  g3->Draw("ALP");
  g3->GetXaxis()->SetTitle(" Energy [MeV]");
  // TGraph * g4 = fill_graph0(d,0,1);

  TCanvas * c2 = new TCanvas();
  c2->SetGridx();
  c2->SetGridy();
  TMultiGraph  *mg  = new TMultiGraph();
  mg->Add(g1);
  mg->Add(g3); // Some hard coding
  mg->GetXaxis()->SetTitle(" Energy [MeV]");
  mg->GetYaxis()->SetTitle("Prob.");
  mg->GetHistogram()->SetTitle("XCOM vs. G4 ");
  mg->Draw("ALP");

  TCanvas* c4 = new TCanvas();
  c4->SetGridx();
  c4->SetGridy();
  TMultiGraph  *mg_e  = new TMultiGraph();
  mg_e->Add(g5);
  mg_e->Add(g4);
  mg_e->GetXaxis()->SetTitle(" Energy [MeV]");
  mg_e->GetYaxis()->SetTitle("Prob. Error");
  mg_e->GetHistogram()->SetTitle("XCOM-G4[Red] vs. G4 Binomial [Black]");
  mg_e->Draw("ALP");
  

}