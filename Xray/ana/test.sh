#!/bin/sh
# Comment
# Author: Shiwen An
# Data: 2021.10.29

helpFunction()
{
   echo "Usage: ./test.sh -m <material>"
   exit 1
}
echo "EPDL Database Calculation"

# python3 endf_read.py
# root test_Al.cxx
# modify the test based on type of material 
# python3 sequence.py -i xcom_Al.txt -o ../run2_E.mac

while getopts "m:" arg;
do
   case "${arg}" in
      m ) material=${OPTARG} 
	  echo $material
	  ;;
      ? ) helpFunction ;;
   esac
done  

if [ "$material" = "Al" ]
then
python3 endf_read.py -i epdl_Al.txt
python3 sequence.py -i epdl_Al.txt -o ../run2_E.mac -m Al
cd ../src
rm HNDetectorConstruction.cc
cat Al.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Al.txt
root test_Al.cxx
fi

if [ "$material" = "Cu" ]
then
python3 endf_read.py -i epdl_Cu.txt
python3 sequence.py -i epdl_Cu.txt -o ../run2_E.mac -m Cu
cd ../src
rm HNDetectorConstruction.cc
cat Cu.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Cu.txt
root test_Cu.cxx
fi

if [ "$material" = "Styro" ]
then
python3 sequence.py -i xcom_C8H8.txt -o ../run2_E.mac -m Styro
cd ../src
rm HNDetectorConstruction.cc
cat Styro.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Styro.txt
root test_Styrofoam.cxx
fi

if [ "$material" = "Kapton" ]
then
python3 sequence.py -i xcom_C23N2O5.txt -o ../run2_E.mac -m Kapton
cd ../src
rm HNDetectorConstruction.cc
cat Kapton.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Kapton.txt
root test_Kapton.cxx
fi

if [ "$material" = "Delrin" ]
then
python3 sequence.py -i xcom_CH2O.txt -o ../run2_E.mac -m Delrin
cd ../src
rm HNDetectorConstruction.cc
cat Delrin.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Delrin.txt
root test_Delrin.cxx
fi