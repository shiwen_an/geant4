// Author: Shiwen An
// Date: 2021.10.25
// Purpose: The library helps analyze the data from database and do the comparison
// Nothing Special

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#define PI 3.1415926

const bool DEBUG_ON = false;
const int NOFEVENT = 1000;     // Number of beam at each energy level
const double ENERGY_STEP = 0.1;  // starts from atan (0.01/1) rad

// C8H8 Density: 0.909 g/cm^3
// Thickness: 30 mm ~ 3 cm
// Fraction by Weight: Z=1 0.077421
// Fraction by Weight: Z=6 0.922579

const double THICKNESS = 2.727; // The thickness of the Styrofoam 3cm* 0.909g/cm^3 
const double THICKNESS_AL = 0.054; // The thickness of the Alumininum 0.02cm * 2.7g/cm^3 
const double THICKNESS_CU = 0.01792; // The thickness of the Alumininum 0.002cm * 8.96g/cm^3 
const double THICKNESS_KAPTON = 0.0284; // The thickness of Kapton
                                        // Kapton Density 1.42 g/cm^3
                                        // thickness 0.2mm 0.02cm

const double THICKNESS_STY = 2.727; // The thickness of the Sty 3cm*0.909 g/cm^3
const double THICKNESS_DELRIN = 0.566 ; // The thickness of the Delrin 0.4cm*1.415 g/cm^3

// The new thickness calculation with molar mass & barn & Avogadro's constant 
// NA = 6.02214086e23
const double THICKNESS_AL_EPDL = THICKNESS_AL * 0.02231948615;  // 6.02214086E23/26.981539*1e-24
const double THICKNESS_CU_EPDL = THICKNESS_CU * 0.009476821295; // 6.02214086E23/63.546*1e-24
// For other Compound I still choose to use the original XCOM database for convenience 
// [Enought Precision]


using namespace std;
void display(vector<vector<double>> & d);
void sequence_generation(vector<vector<double>> & d);
vector<TGraph*> root_plot(vector<vector<double>> & d, double thickness);
vector<TGraph*> root_plot(vector<vector<double>> & d, string ss); // Change based on different database and material

vector<vector<double>> parse2DCsvFile(string fileName);
vector<vector<double>> parse2DCsvFile0(string fileName);
vector<vector<double>> parse2DCsvFile_epdl (string fileName);

vector<vector<double>> add_up_step (vector<vector<double>>& d);
vector<vector<double>> add_up_event(vector<vector<double>> & d,vector<vector<double>> & a);
vector<vector<double>> add_up_event_epdl(vector<vector<double>> & d,vector<vector<double>> & a);
TGraph * fill_graph(vector<vector<double>> &d);
TGraph * fill_graph0(vector<vector<double>> &d, int col1, int col2);
TGraph * fill_difference(vector<vector<double>> &d, vector<vector<double>> &d2);
TGraph * fill_difference_epdl(vector<vector<double>> &d, vector<vector<double>> &d2);
TGraph * fill_graph_err(vector<vector<double>> &d);




TGraph * fill_difference(vector<vector<double>> &d, vector<vector<double>> &d2, double thickness){
  Int_t n = d.size();
  double sum;
  Double_t x[n], y1[n], y2[n], y3[n], y4[n],
          y5[n], y6[n], y7[n], y_sum[n], y_diff[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0];
    y1[i] = 1-exp(-d[i][1]*thickness) ;
    y2[i] = 1-exp(-d[i][2]*thickness) ;
    y3[i] = 1-exp(-d[i][3]*thickness) ;
    y4[i] = 1-exp(-d[i][4]*thickness) ;
    y5[i] = 1-exp(-d[i][5]*thickness) ;
    y6[i] = 1-exp(-d[i][6]*thickness) ;
    y7[i] = 1-exp(-d[i][7]*thickness) ;
    y_diff[i] = y7[i]-d2[i][1]; //XCOM - Simulation
  }
  TGraph *gr1 = new TGraph(n,x,y_diff);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(2);
  return gr1;
}

TGraph * fill_difference_epdl(vector<vector<double>> &d, vector<vector<double>> &d2, double thickness){
  Int_t n = d.size();
  double sum;
  Double_t x[n], y7[n], y_diff[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0]*1e-6;
    y7[i] = 1-exp(-d[i][1]*thickness) ;
    y_diff[i] = y7[i]-d2[i][1]; //EPDL - Simulation
  }
  TGraph *gr1 = new TGraph(n,x,y_diff);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(2);
  return gr1;
}

TGraph * fill_graph0(vector<vector<double>> &d, int col1, int col2){
  int s = d.size();
  double x[s], y[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][col1]; // Energy 
    y[i] = d[i][col2]; // Hit
    if(DEBUG_ON) cout<<x[i]<<y[i]<<"\n";
  }
  TGraph * g = new TGraph(s,x,y);
  return g;
}

TGraph * fill_graph(vector<vector<double>> &d){
  int s = d.size();
  double x[s], y[s], ex[s],ey[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][0]; // Energy 
    y[i] = d[i][1]; // Hit Probability
    ex[i] = 0 ;
    ey[i] = sqrt( double(y[i]*(1.0-y[i])/1000.0));
    cout<<ey[i]<<"\n";
  }
  TGraph * g = new TGraphErrors(s,x,y,ex,ey);
  return g;
}

TGraph * fill_graph_err(vector<vector<double>> &d){
  int s = d.size();
  double x[s], y[s], ex[s],ey[s];
  for(int i = 0 ; i<s ; i++){
    x[i] = d[i][0]; // Energy 
    y[i] = d[i][1]; // Hit Probability
    ex[i] = 0 ;
    ey[i] = sqrt( double(y[i]*(1.0-y[i])/1000.0));
    cout<<ey[i]<<"\n";
  }
  TGraph * g = new TGraph(s,x, ey);
  g->SetMarkerStyle(kFullTriangleDown); // With Triangle
  g->SetMarkerColor(1);
  return g;
}


vector<vector<double>> add_up_event_epdl(vector<vector<double>> & d, vector<vector<double>> & a){
  int s = d.size();
  int index_e = 1; // energy index 
  int h = 0;       // # of hit
  double tmp  = 0;
  vector<vector<double>> d1;
  for(int i = 0; i<s; i++){
    if(i == (s-1)){
      tmp += d[i][1];
      if(d[i][1] >0) h++; // hit then h++
      
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp} );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      break;
    }

    if(i==0){
      tmp = d[i][1];
      if(tmp>0) h = 1;
      else h= 0;  // hit then h++
    }else if(d[i][0]>d[i-1][0] ){
      if (d[i][1] > 0){tmp+=d[i][1];
        h++;
      }
    }else if(d[i][0]<d[i-1][0]){
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp } );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      index_e++;
      tmp = d[i][1];
      if(tmp>0) h = 1; else h= 0;  // hit then h++
    }
  }
  for(int j = 0; j<d1.size(); j++){
    if(DEBUG_ON) {
      std::cout<<"At Energy "<<d1[j][2]<<" Energy Deposit " << d1[j][1] 
               <<" and hit/miss " << d1[j][0] <<"\n"; 
    }
  }
  int x1 = a.size();
  int x2 = d1.size();
  if( x1 == x2 ) std::cout<<"Great!"<<endl;
  for(int i =0; i<x1; i++){
    d1[i][0] = a[i][0]*1e-6;
  }
  return d1;
}

vector<vector<double>> add_up_event(vector<vector<double>> & d, vector<vector<double>> & a){
  int s = d.size();
  int index_e = 1; // energy index 
  int h = 0;       // # of hit
  double tmp  = 0;
  vector<vector<double>> d1;
  for(int i = 0; i<s; i++){
    if(i == (s-1)){
      tmp += d[i][1];
      if(d[i][1] >0) h++; // hit then h++
      
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp} );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      break;
    }

    if(i==0){
      tmp = d[i][1];
      if(tmp>0) h = 1;
      else h= 0;  // hit then h++
    }else if(d[i][0]>d[i-1][0] ){
      if (d[i][1] > 0){tmp+=d[i][1];
        h++;
      }
    }else if(d[i][0]<d[i-1][0]){
      if(h<NOFEVENT) d1.push_back( {0, (double) h/(NOFEVENT), tmp } );
      else if (h >= NOFEVENT) d1.push_back( {0, 1, tmp} );
      index_e++;
      tmp = d[i][1];
      if(tmp>0) h = 1; else h= 0;  // hit then h++
    }
  }
  for(int j = 0; j<d1.size(); j++){
    if(DEBUG_ON) {
      std::cout<<"At Energy "<<d1[j][2]<<" Energy Deposit " << d1[j][1] 
               <<" and hit/miss " << d1[j][0] <<"\n"; 
    }
  }
  int x1 = a.size();
  int x2 = d1.size();
  if( x1 == x2 ) std::cout<<"Great!"<<endl;
  for(int i =0; i<x1; i++){
    d1[i][0] = a[i][0];
  }
  return d1;
}

vector<vector<double>> add_up_step (vector<vector<double>>& d) {

  int s = d.size();  
  vector<vector<double >> pb; 
  int a = 0 ; 
  double tmp = 0; 
  for (int i =0; i< s; i++){
      if(d[a][0] == d[i][0]){
        tmp += d[i][1];
      }
      else{
        pb.push_back({d[a][0], tmp});
        a=i;
        tmp = d[i][1];
      }
      if(i==(s-1)) pb.push_back({d[i][0],tmp});
  }
  int counter = 0 ;
  for(int i = 0; i< pb.size() ;i++){
    if(DEBUG_ON) {
        cout<<"Loop event"  <<pb[i][0]<< " and Value " << pb[i][1]<< " at index "
        << i <<"\n" ;
        if(pb[i][0 ] == 98 ) {
          counter++;
          cout<<"\n" <<"Energy Level at" <<counter<< "\n";
        }
    }
  }

  return pb;
}

// Photon  [1]Coherent [2]Incoher. [3]Photoel. [4]Nuclear  [5]Electron [6]Tot. w/  [7]Tot. wo/ 
// Energy     Scatter.    Scatter.    Absorb.     Pr. Prd.    Pr. Prd.    Coherent    Coherent
vector<TGraph*> root_plot(vector<vector<double>> & d, double thickness){
  
  Int_t n = d.size();
  double sum;
  Double_t x[n], y1[n], y2[n], y3[n], y4[n],
          y5[n], y6[n], y7[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0];
    y1[i] = 1-exp(-d[i][1]*thickness) ;
    y2[i] = 1-exp(-d[i][2]*thickness) ;
    y3[i] = 1-exp(-d[i][3]*thickness) ;
    y4[i] = 1-exp(-d[i][4]*thickness) ;
    y5[i] = 1-exp(-d[i][5]*thickness) ;
    y6[i] = 1-exp(-d[i][6]*thickness) ;
    y7[i] = 1-exp(-d[i][7]*thickness) ;
  }
  
  TGraph *gr1 = new TGraph(n,x,y1);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(1);

  TGraph *gr2 = new TGraph(n,x,y2);
  gr2->SetMarkerStyle(21); // With Square
  gr2->SetMarkerColor(2);
  TGraph *gr3 = new TGraph(n,x,y3);
  gr3->SetMarkerStyle(21); // With Square
  gr3->SetMarkerColor(3);
  
  TGraph *gr4 = new TGraph(n,x,y4);
  gr4->SetMarkerStyle(21); // With Square
  gr4->SetMarkerColor(4);

  TGraph *gr5 = new TGraph(n,x,y5);
  gr5->SetMarkerStyle(21); // With Square
  gr5->SetMarkerColor(5);

  TGraph *gr6 = new TGraph(n,x,y6);
  gr6->SetMarkerStyle(21); // With Square
  gr6->SetMarkerColor(6);

  TGraph *gr7 = new TGraph(n,x,y7);
  gr7->SetMarkerStyle(21); // With Square
  gr7->SetMarkerColor(7);

  vector<TGraph*> gr_list;
  gr_list.push_back(gr1);
  gr_list.push_back(gr2);
  gr_list.push_back(gr3);
  gr_list.push_back(gr4);
  gr_list.push_back(gr5);
  gr_list.push_back(gr6);
  gr_list.push_back(gr7);
  std::cout<<endl;

  if(DEBUG_ON){
  TMultiGraph  *mg  = new TMultiGraph();
    mg->Add(gr1);
    mg->Add(gr2);
    mg->Add(gr3);
    mg->Add(gr4);
    mg->Add(gr5);
    mg->Add(gr6);
    mg->Add(gr7);
    mg->Draw("ALP");
  }
  return gr_list;
}

// here actual calculation is using barn 
// Additional conversion is necessary
// 1barn = 10^-28 m^2 = 10^-24 cm^2
vector<TGraph*> root_plot_epdl(vector<vector<double>> & d, double thickness){
  Int_t n = d.size();
  double sum;
  Double_t x[n], y1[n];
  for(int i = 0; i<n; i++){
    x[i] = d[i][0]*1e-6;
    y1[i] = 1-exp(-d[i][1]* thickness) ;
  }
  TGraph *gr1 = new TGraph(n,x,y1);
  gr1->SetMarkerStyle(21); // With Square
  gr1->SetMarkerColor(1);

  vector<TGraph*> gr_list;
  gr_list.push_back(gr1);
  std::cout<<endl;
  return gr_list;
}

// Generate Energy For the Plot
void sequence_generation(vector<vector<double>> & d){
  std::filebuf fb;
  fb.open ("../run2_E.mac",std::ios::out);
  std::ostream os(&fb);
  // Energy following the data
  for(int i= 0; i< d.size(); i++){
    os<<"/gun/energy "<<d[i][0]<<" MeV"<<endl;
    os<<"/run/beamOn 1000"<<endl;
  }
  fb.close();
}


void display(vector<vector<double>> & d){
  int s = d.size();
  int s_r = d[0].size();
  for(int n= 0 ; n<s; n++){
    for(int m = 0 ; m<s_r; m++){
      std::cout<<d[n][m]<<" ";
    }
    std::cout<<std::endl;
  }
}

// Common Parse Function for comma txt file
vector<vector<double>> parse2DCsvFile0(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ','))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}

vector<vector<double>> parse2DCsvFile(string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ' '))
                    break;
                try {
                    record.push_back(stof(line));
                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}

vector<vector<double>> parse2DCsvFile_epdl (string fileName) {
    vector<vector<double> > data;
    ifstream inputFile(fileName);
    int l = 0;
    while (inputFile) {
        l++;
        string s;
        if (!getline(inputFile, s)) break;
        if (s[0] != '#') {
            istringstream ss(s);
            vector<double> record;
 
            while (ss) {
                string line;
                if (!getline(ss, line, ' '))
                    break;
                try {

                    record.push_back(stof(line));

                }
                catch (const std::invalid_argument e) {
                    cout << "NaN found in file " << fileName << " line " << l
                         << endl;
                    e.what();
                }
            }
 
            data.push_back(record);
        }
    }
 
    if (!inputFile.eof()) {
        cerr << "Could not read file " << fileName << "\n";
        __throw_invalid_argument("File not found.");
    }
 
    return data;
}
